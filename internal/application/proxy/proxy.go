package proxy

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/Murrengan/murr_proxy/internal/config"
	"gitlab.com/Murrengan/murr_proxy/internal/handlers"
	"gitlab.com/Murrengan/murr_proxy/internal/providers/murr_card"
	"gitlab.com/Murrengan/murr_proxy/pkg/observer"
	"gitlab.com/Murrengan/murr_proxy/pkg/server"
	"gitlab.com/Murrengan/murr_proxy/pkg/tracing"
	"go.uber.org/zap"
)

func Run() error {
	logger, err := zap.NewProduction()
	if err != nil {
		return fmt.Errorf("failed to create logger: %v", err)
	}
	defer logger.Sync() // nolint:errcheck
	appLogger := logger.Named("murrengan_proxy")

	cfg, err := config.LoadConfiguration()
	if err != nil {
		appLogger.Error("Error loading configuration", zap.Error(err))
		return fmt.Errorf("loading configuration: %v", err)
	}

	murrCardGRPCClient, err := murrcard.NewMurrCardService(cfg.MurrCard.Address)
	if err != nil {
		appLogger.Error("Error creating MurrCard gRPC client", zap.Error(err))
		return fmt.Errorf("creating MurrCard gRPC client: %v", err)
	}

	healthMetricsHandler := tracing.NewGracefulMetricsServer()
	metricsServer := &http.Server{
		Addr:    ":9002",
		Handler: healthMetricsHandler,
	}
	httpMetricsServer := server.NewGracefulServer(metricsServer, logger)

	rootRouter := mux.NewRouter()

	loggingMiddleware := handlers.NewLoggingMiddleware(logger)
	loggingMiddleware.Apply(rootRouter)

	murrCardHandler := handlers.NewMurrCardHandler(murrCardGRPCClient)
	murrCardHandler.Register(rootRouter)

	handlerWithCors := handlers.ApplyCors(rootRouter)

	murrCardServer := &http.Server{
		Addr:    fmt.Sprintf(":%d", cfg.Server.Port),
		Handler: handlerWithCors,
	}
	httpMurrCardServer := server.NewGracefulServer(murrCardServer, logger)

	obs := observer.NewObserver()

	obs.AddOpener(observer.OpenerFunc(func() error {
		return httpMetricsServer.Serve()
	}))

	obs.AddOpener(observer.OpenerFunc(func() error {
		return httpMurrCardServer.Serve()
	}))

	obs.AddContextCloser(observer.ContextCloserFunc(func(ctx context.Context) error {
		return httpMetricsServer.Shutdown(ctx)
	}))

	obs.AddContextCloser(observer.ContextCloserFunc(func(ctx context.Context) error {
		return httpMurrCardServer.Shutdown(ctx)
	}))

	obs.AddUpper(func(ctx context.Context) {
		select {
		case <-ctx.Done():
		case <-httpMetricsServer.Dead():
		case <-httpMurrCardServer.Dead():
		}
	})

	return obs.Run()

}
