package murrcard

import (
	murrcardpb "gitlab.com/Murrengan/murr_api/inner/murrcard/v1"
	"gitlab.com/Murrengan/murr_proxy/internal/domain"
)

func protoToMurrCardOut(src *murrcardpb.MurrCard) domain.MurrCardOUT {
	return domain.MurrCardOUT{
		ID:        src.GetId(),
		MurrenID:  src.GetMurrenId(),
		Title:     src.GetTitle(),
		Content:   src.GetContent(),
		CreatedAt: src.GetCreatedAt().AsTime(),
		UpdatedAt: src.GetUpdatedAt().AsTime(),
		Likes:     src.GetLikes(),
		Dislikes:  src.GetDislikes(),
	}
}

func protoToMurrCardsOut(src []*murrcardpb.MurrCard) []domain.MurrCardOUT {
	dst := make([]domain.MurrCardOUT, len(src))
	for i, v := range src {
		dst[i] = protoToMurrCardOut(v)
	}
	return dst
}

func murrCardInToProto(src domain.MurrCardIN) *murrcardpb.MurrCard {
	resp := &murrcardpb.MurrCard{
		Title:   src.Title,
		Content: src.Content,
	}
	return resp
}

func rateTypeToProto(src domain.VoteType) murrcardpb.RateType_Enum {
	switch src {
	case domain.VoteTypeLike:
		return murrcardpb.RateType_LIKE
	case domain.VoteTypeDislike:
		return murrcardpb.RateType_DISLIKE
	default:
		return murrcardpb.RateType_LIKE
	}
}
