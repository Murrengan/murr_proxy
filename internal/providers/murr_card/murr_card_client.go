package murrcard

import (
	"context"
	"fmt"

	murrcardpb "gitlab.com/Murrengan/murr_api/inner/murrcard/v1"
	"gitlab.com/Murrengan/murr_proxy/internal/domain"
	grpc2 "gitlab.com/Murrengan/murr_proxy/pkg/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func NewMurrCardServiceClient(addr string) (murrcardpb.MurrCardServiceClient, error) {
	conn, err := grpc2.NewGrpcClient(addr)
	if err != nil {
		return nil, fmt.Errorf("create grpc client: %v", err)
	}
	return murrcardpb.NewMurrCardServiceClient(conn), nil
}

type Service struct {
	client murrcardpb.MurrCardServiceClient
}

func NewMurrCardService(addr string) (*Service, error) {
	client, err := NewMurrCardServiceClient(addr)
	if err != nil {
		return nil, err
	}
	return &Service{client: client}, nil
}

func (s *Service) GetCard(ctx context.Context, cardID string) (domain.MurrCardOUT, error) {
	req := &murrcardpb.GetMurrCardRequest{Id: cardID}
	resp, err := s.client.GetMurrCard(ctx, req)
	if err != nil {
		st, ok := status.FromError(err)
		if !ok {
			return domain.MurrCardOUT{}, fmt.Errorf("get card: %v", err)
		}
		if st.Code() == codes.NotFound {
			return domain.MurrCardOUT{}, domain.NewError(domain.NotFound, "")
		}
		return domain.MurrCardOUT{}, fmt.Errorf("get card: %v", err)
	}
	return protoToMurrCardOut(resp), nil
}

func (s *Service) ListCards(ctx context.Context, limit, offset int32) (domain.MurrCardOUTList, error) {
	req := &murrcardpb.ListMurrCardsRequest{Limit: limit, Offset: offset}
	resp, err := s.client.ListMurrCards(ctx, req)
	if err != nil {
		return domain.MurrCardOUTList{}, fmt.Errorf("list cards: %v", err)
	}

	result := domain.MurrCardOUTList{
		Cards:    protoToMurrCardsOut(resp.GetMurrCards()),
		Limit:    resp.GetLimit(),
		Offset:   resp.GetOffset(),
		Total:    resp.GetTotalCount(),
		NextPage: resp.GetNextPage(),
	}
	return result, nil
}

func (s *Service) CreateCard(ctx context.Context, card domain.MurrCardIN) (domain.MurrCardOUT, error) {
	resp, err := s.client.CreateMurrCard(ctx, murrCardInToProto(card))
	if err != nil {
		return domain.MurrCardOUT{}, fmt.Errorf("create card: %v", err)
	}
	return protoToMurrCardOut(resp), nil
}

func (s *Service) RateCard(ctx context.Context, cardID string, rateType domain.VoteType) error {
	req := &murrcardpb.RateMurrCardRequest{MurrCardId: cardID, Rate: rateTypeToProto(rateType)}
	_, err := s.client.RateMurrCard(ctx, req)
	if err != nil {
		return fmt.Errorf("rate card: %v", err)
	}
	return nil
}
