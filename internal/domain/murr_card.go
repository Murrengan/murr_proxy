package domain

import (
	"strings"
	"time"
)

type MurrCardOUTList struct {
	Cards    []MurrCardOUT `json:"cards"`
	Limit    int32         `json:"limit"`
	Offset   int32         `json:"offset"`
	Total    int32         `json:"total"`
	NextPage bool          `json:"next_page"`
}

type MurrCardOUT struct {
	ID        string    `json:"id"`
	MurrenID  string    `json:"murren_id"`
	Title     string    `json:"title"`
	Content   string    `json:"content"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	Likes     int64     `json:"likes"`
	Dislikes  int64     `json:"dislikes"`
}

type MurrCardIN struct {
	ID      string `json:"-"`
	Title   string `json:"title"`
	Content string `json:"content"`
}

type VoteType string

const (
	VoteTypeLike    VoteType = "LIKE"
	VoteTypeDislike VoteType = "DISLIKE"
	VoteTypeUnknown VoteType = "UNKNOWN"
)

func (v VoteType) String() string {
	return string(v)
}

func VoteTypeFromString(s string) VoteType {
	switch strings.ToUpper(s) {
	case "LIKE":
		return VoteTypeLike
	case "DISLIKE":
		return VoteTypeDislike
	default:
		return VoteTypeUnknown
	}
}
