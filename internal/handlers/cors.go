package handlers

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

func ApplyCors(router *mux.Router) http.Handler {
	c := cors.New(cors.Options{
		AllowCredentials: true,
		Debug:            false,
	})

	handler := c.Handler(router)
	return handler
}
