package handlers

import (
	"net/http"

	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

type LoggingMiddleware struct {
	logger *zap.Logger
}

func NewLoggingMiddleware(logger *zap.Logger) *LoggingMiddleware {
	return &LoggingMiddleware{logger: logger.Named("server_log")}
}

func (a *LoggingMiddleware) Apply(router *mux.Router) {
	router.Use(a.Handle)
}

func (a *LoggingMiddleware) Handle(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		a.logger.Info("access",
			zap.String("method", r.Method),
			zap.String("host", r.Host),
			zap.String("uri", r.RequestURI),
			zap.String("remote", r.RemoteAddr),
			zap.String("url", r.URL.String()),
		)
		next.ServeHTTP(w, r)
	})
}
