package handlers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/Murrengan/murr_proxy/internal/domain"
)

type MurrCardHandler struct {
	baseHandler
	cards MurrCardProvider
}

func NewMurrCardHandler(cards MurrCardProvider) *MurrCardHandler {
	return &MurrCardHandler{
		cards: cards,
	}
}

func (h *MurrCardHandler) Register(router *mux.Router) {
	router.HandleFunc("/murr_card", h.listCards).Methods("GET")
	router.HandleFunc("/murr_card/{cardID}", h.getCard).Methods("GET")
	router.HandleFunc("/murr_card/{cardID}/rate/{rate_type}", h.rateCard).Methods("POST")
	router.HandleFunc("/murr_card", h.createCard).Methods("POST")
}

func (h *MurrCardHandler) getCard(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	cardID := vars["cardID"]
	if cardID == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	card, err := h.cards.GetCard(r.Context(), cardID)

	if domain.NotFound.Is(err) {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	h.RespJSON(w, card, http.StatusOK)
}

func (h *MurrCardHandler) listCards(w http.ResponseWriter, r *http.Request) {
	limit, offset := h.getLimitOffset(r)

	cards, err := h.cards.ListCards(r.Context(), limit, offset)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	jsonCards, err := json.Marshal(cards)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(jsonCards)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (h *MurrCardHandler) createCard(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		h.ErrorJSON(w, "", http.StatusInternalServerError)
	}
	newCard := domain.MurrCardIN{}
	err = json.Unmarshal(body, &newCard)
	if err != nil {
		h.ErrorJSON(w, "invalid body", http.StatusBadRequest)
		return
	}

	card, err := h.cards.CreateCard(r.Context(), newCard)
	if err != nil {
		h.ErrorJSON(w, "", http.StatusInternalServerError)
		return
	}

	h.RespJSON(w, card, http.StatusCreated)
}

func (h *MurrCardHandler) rateCard(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	cardID := vars["cardID"]
	if cardID == "" {
		h.ErrorJSON(w, "", http.StatusBadRequest)
		return
	}

	rateType := vars["rate_type"]
	if rateType == "" {
		h.ErrorJSON(w, "", http.StatusBadRequest)
		return
	}

	err := h.cards.RateCard(r.Context(), cardID, domain.VoteTypeFromString(rateType))
	if err != nil {
		h.ErrorJSON(w, "", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}
