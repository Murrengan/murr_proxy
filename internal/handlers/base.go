package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"
)

type baseHandler struct {
}

func (b *baseHandler) getLimitOffset(r *http.Request) (int32, int32) {
	limit, err := strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		limit = 20
	}

	offset, err := strconv.Atoi(r.URL.Query().Get("offset"))
	if err != nil {
		offset = 0
	}

	if limit > 20 || limit < 1 {
		limit = 20
	}
	if offset < 0 {
		offset = 0
	}
	return int32(limit), int32(offset)
}

func (b *baseHandler) ErrorJSON(w http.ResponseWriter, error string, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	if error != "" {
		w.Write([]byte(`{"error":"` + error + `"}`)) // nolint: errcheck
	}
	w.WriteHeader(code)
}

func (b *baseHandler) RespJSON(w http.ResponseWriter, body interface{}, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(code)
	if body != nil {
		jsonBody, err := json.Marshal(body)
		if err != nil {
			b.ErrorJSON(w, "", http.StatusInternalServerError)
			return
		}
		w.Write(jsonBody) // nolint: errcheck
	}
}
