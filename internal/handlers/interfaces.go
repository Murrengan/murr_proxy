package handlers

import (
	"context"

	"gitlab.com/Murrengan/murr_proxy/internal/domain"
)

type MurrCardProvider interface {
	GetCard(ctx context.Context, cardID string) (domain.MurrCardOUT, error)
	ListCards(ctx context.Context, limit, offset int32) (domain.MurrCardOUTList, error)
	CreateCard(ctx context.Context, card domain.MurrCardIN) (domain.MurrCardOUT, error)
	RateCard(ctx context.Context, cardID string, rateType domain.VoteType) error
}
