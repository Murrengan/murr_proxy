package config

import (
	"github.com/caarlos0/env/v6"
)

type Configuration struct {
	MurrCard MurrCardConfig
	Server   ServerConfig
}

type MurrCardConfig struct {
	Address string `env:"MURRCARD_ADDRESS" envDefault:"murrcard:8080" required:"true"`
}

type ServerConfig struct {
	Port int `env:"SERVER_PORT" envDefault:"8080"`
}

func LoadConfiguration() (*Configuration, error) {
	var config Configuration
	if err := env.Parse(&config); err != nil {
		return nil, err
	}
	return &config, nil
}
