FROM golang:alpine AS builder
RUN apk update && apk add --no-cache ca-certificates git gcc make libc-dev binutils-gold
RUN mkdir murrengan
WORKDIR murrengan
COPY . .
RUN go get -d -v

RUN CGO_ENABLED=0 GOARCH=amd64 GOOS=linux go build -trimpath -o /bin/proxy ./cmd/proxy


FROM scratch
COPY --from=builder /bin/proxy /bin/proxy
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

ENV MIGRATIONS_DIR /migrations

ENTRYPOINT ["/bin/proxy"]