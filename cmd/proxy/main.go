package main

import "gitlab.com/Murrengan/murr_proxy/internal/application/proxy"

func main() {
	err := proxy.Run()
	if err != nil {
		panic(err)
	}
}
