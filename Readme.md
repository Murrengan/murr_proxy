# MurrCard proxy server:

- Prometheus metrics exposed on localhost:9000/metrics
- Liveliness check exposed on localhost:9000/alive

##### Launch
`docker run imagename:tagname`

- #### ENVIRONMENT VARIABLES
```dotenv
SERVER_PORT=8080 #Default: 8080
MURRCARD_ADDRESS=murrcard:8080 #Default: murrcard:8080
TRACING_JAEGER_ENDPOINT=localhost:14268 #Default: -- 
TRACING_TRACER_NAME=proxy #Default: proxy
```
